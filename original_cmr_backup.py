import requests
from requests.auth import HTTPBasicAuth
import sys
#from config import user_name, pwd


def check_ticket_status(url, issue_key, username, password, desired_status):
    # Jira REST API endpoint for getting issue details
    url = url
    # Set the request headers and body
    headers = {
        "Content-Type": "application/json"
    }
    try:
        # Send a GET request to the Jira issue endpoint with basic authentication
        response = requests.get(url, headers=headers, auth=HTTPBasicAuth(username, password))
        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            issue = response.json()
            status = issue["fields"]["status"]["name"]
            print("[INFO] Actual status inside the 'check_ticket_status' method:", status.lower())
            print("[INFO] Expected status inside the 'check_ticket_status' method:", desired_status.lower())
            if desired_status.lower() in status.lower():
                return True
            else:
                return False
        else:
            print(f"[INFO] Failed to retrieve issue details. Status code: {response.status_code}")
    except requests.exceptions.RequestException as e:
        print(f"An error occurred: {e}")


def add_comment_to_jira(url, comment_body, username, password):
    api_url = url
    headers = {
        "Content-Type": "application/json"
    }
    data = {
        "body": comment_body
    }

    try:
        response = requests.post(api_url, headers=headers, auth=HTTPBasicAuth(username, password), json=data)
        if response.status_code == 201:
            print("[INFO] Jira comment added successfully.")
        else:
            print(f"[INFO] Failed to add Jira comment. Status code: {response.status_code}, Error message: {response.text}")
    except requests.exceptions.RequestException as e:
        print(f"Error occurred: {e}")


def main(project, user_name, pwd, ticket_id, desired_status, pipeline_url, pipeline_triggered_by):
    project = project
    # user_name = user_name
    # pwd = pwd
    user_name = user_name
    pwd = pwd
    ticket_id = ticket_id
    base_url = "https://lendingkart.atlassian.net"
    url_check_status = f"{base_url}/rest/api/2/issue/{ticket_id}"
    url_post_comment = f"{base_url}/rest/api/2/issue/{ticket_id}/comment"
    pipline_url = pipeline_url
    pipeline_triggered_by = pipeline_triggered_by
    desired_status = desired_status
    if 'approved' in desired_status.lower():
        comment_to_post = f"Gitlab comment - The pipline '{pipline_url}' was triggered by '{pipeline_triggered_by}' for 'production deployment'"
    elif 'rollback' in desired_status.lower():
        comment_to_post = f"Gitlab comment - The pipline '{pipline_url}' was triggered by '{pipeline_triggered_by}' for 'production rollback'"
    else:
        comment_to_post = f"Gitlab comment - The pipline '{pipline_url}' was triggered by '{pipeline_triggered_by}' for '{desired_status}'"

    ticket_current_status = check_ticket_status(url_check_status, ticket_id, user_name, pwd, desired_status)
    add_comment_to_jira(url_post_comment, comment_to_post, user_name, pwd)
    if ticket_current_status:
        print(f"[INFO] The current status of the ticket {ticket_id} is '{desired_status}'.")
        return True
    else:
        print(f"[INFO] The current status of the ticket {ticket_id} is NOT '{desired_status}'.")
        return False



if __name__ == '__main__':
    project = "CMR"
    ticket_id = sys.argv[1]
    if "cmr-" not in ticket_id.lower():
        raise RuntimeError(f"[ERROR] Invalid or Empty CMR TicketID. It seems that CMR TicketID was not provided in the Commit Msg/Merge Request title.")
    desired_status = sys.argv[2]
    CI_PIPELINE_URL = sys.argv[3]
    GITLAB_USER_EMAIL = sys.argv[4]
    JIRA_USER_NAME = sys.argv[5]
    JIRA_API_TOKEN = sys.argv[6]
    if "rollback" not in desired_status.lower() and "approved" not in desired_status.lower():
        raise RuntimeError(f"[ERROR] Invalid desired status. It must be either 'Approved' or 'Rollback Required', but the provided status is '{desired_status}'.")
    url = f"https://lendingkart.atlassian.net/rest/api/2/issue/{ticket_id}"

    final_ticket_status = main(project, JIRA_USER_NAME, JIRA_API_TOKEN, ticket_id, desired_status, CI_PIPELINE_URL, GITLAB_USER_EMAIL)

    if not final_ticket_status:
        # sys.exit(1)
        raise RuntimeError(f"[ERROR] The current status of the ticket {ticket_id} is NOT '{desired_status}'.")
