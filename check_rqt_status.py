import sys

def check_rqt_status(failure_count):
    if failure_count.isdigit() and int(failure_count) > 0:
        return False
    else:
        return True

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Please provide the failure count as a command-line argument")
        sys.exit(1)

    failure_count = sys.argv[1]
    rqt_status = check_rqt_status(failure_count)
    if not rqt_status:
        print("[ERROR] RQT is not PASSED. RQT status check is Failed. Aborting prod-deployment action!")
        sys.exit(1)
    else:
        print("[INFO] RQT status check is Successful. Proceeding with prod-deployment action!")

