import datetime
class RqtWindow:
    @staticmethod
    def time_in_range(start, end, current):
        return start <= current <= end
    @classmethod
    def main(cls,rqt_window):
        output_list=[]
        for time_range_list in rqt_window:
            print("time_range_list", time_range_list)
            start_time = time_range_list[0]
            #print("start_time", start_time)
            end_time = time_range_list[1]
            #print("end_time", end_time)
            start = datetime.time(start_time[0], start_time[1])
            print("start_time", start)
            end = datetime.time(end_time[0], end_time[0])
            print("end_time", end)
            current = datetime.datetime.now().time()
            print("current_time", current)
            output = RqtWindow.time_in_range(start, end, current)
            print("output:", output)
            output_list.append(output)
        if True in output_list:
            return True
        else:
            return False
if __name__ == '__main__':
    ########### To block only during RQT run ###########
    #rqt_window = [[(4, 20), (5, 40)], [(6, 20), (7, 40)], [(8, 20), (9, 40)], [(10, 20), (11, 40)], [(12, 20), (13, 40)]]
    ########### To block from 8am to 10pm ###########
    #rqt_window = [[(14, 10), (16, 30)]]
    ########### To unblock till 11pm ###########
    rqt_window = [[(17, 30), (19, 30)]]
    ########### To unblock for specific time ###########
    #rqt_window = [[(6, 20), (7, 40)]]
    rqt_status = RqtWindow.main(rqt_window)
    print(rqt_status)
