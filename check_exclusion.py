import sys

def check_exclusion(project_name, exclusion_list):
    projects = exclusion_list.split(',')
    return project_name in projects

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python check_exclusion.py <project_name> <exclusion_list>")
        sys.exit(1)

    project_name = sys.argv[1]
    exclusion_list = sys.argv[2]

    if check_exclusion(project_name, exclusion_list):
        print("true")
    else:
        print("false")
